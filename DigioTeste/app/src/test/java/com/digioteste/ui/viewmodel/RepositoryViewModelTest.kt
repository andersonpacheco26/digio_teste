package com.digioteste.ui.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.digioteste.data.RepositoryService
import com.digioteste.data.ServiceRepository
import com.digioteste.model.Repository
import com.nhaarman.mockitokotlin2.any
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertTrue
import okhttp3.OkHttpClient
import okhttp3.Request
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.Call
import retrofit2.Response
import java.io.IOException


@RunWith(MockitoJUnitRunner::class)
class RepositoryViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    private val okHttpClient: OkHttpClient? = null


    @Test
    fun login_Success() {
        val repositoryService: RepositoryService = ServiceRepository.createService(RepositoryService::class.java)
        val call: Call<Repository> = repositoryService.repository()
        try {
            val response: Response<Repository> = call.execute()
            val result: Repository? = response.body()
            assertTrue(response.isSuccessful)
            println(result!!.cash!!.title)
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }


}