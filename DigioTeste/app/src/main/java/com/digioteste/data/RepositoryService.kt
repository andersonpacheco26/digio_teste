package com.digioteste.data

import com.digioteste.model.Repository
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers

interface RepositoryService {
    @Headers("Content-Type: application/json")
    @GET("sandbox/products")
    fun repository(): Call<Repository>
}