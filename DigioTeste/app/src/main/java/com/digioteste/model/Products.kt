package com.digioteste.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Products (
    @SerializedName("name") val name: String?,
    @SerializedName("imageURL") val imageURL: String?,
    @SerializedName("description") val description: String?
) : Parcelable