package com.digioteste.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import com.google.gson.annotations.SerializedName

@Parcelize
data class Cash (
    @SerializedName("title") val title: String?,
    @SerializedName("bannerURL") val bannerURL: String?,
    @SerializedName("description") val description: String?
) : Parcelable