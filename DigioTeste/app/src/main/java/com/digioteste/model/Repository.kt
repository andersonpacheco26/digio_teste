package com.digioteste.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import com.google.gson.annotations.SerializedName

@Parcelize
data class Repository (
    @SerializedName("spotlight") val spotlight: ArrayList<Spotlight>?,
    @SerializedName("products") val products: ArrayList<Products>?,
    @SerializedName("cash") val cash: Cash?
) : Parcelable