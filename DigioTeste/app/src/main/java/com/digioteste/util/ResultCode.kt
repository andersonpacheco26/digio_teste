package com.digioteste.util

enum class ResultCode(val value: Int) {
    SUCESS(200),
    NOTFOUND(404)
}