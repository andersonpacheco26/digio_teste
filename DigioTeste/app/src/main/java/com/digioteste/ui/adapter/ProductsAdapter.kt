package com.digioteste.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.digioteste.R
import com.digioteste.model.Products

class ProductsAdapter(
    private val ctx: Context,
    private val data: ArrayList<Products>) : RecyclerView.Adapter<ProductsAdapter.ProductsHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ProductsHolder {
        val view = LayoutInflater.from(ctx)
            .inflate(R.layout.card_view_products, viewGroup, false)
        return ProductsHolder(view)
    }

    override fun onBindViewHolder(productsHolder: ProductsHolder, i: Int) {
        Glide
            .with(ctx)
            .load(data[i].imageURL)
            .centerCrop()
            .transform(RoundedCorners(32))
            .into(productsHolder.iv_produto)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    inner class ProductsHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val iv_produto: ImageView
        init {
            iv_produto = itemView.findViewById(R.id.iv_produto)
        }
    }
}