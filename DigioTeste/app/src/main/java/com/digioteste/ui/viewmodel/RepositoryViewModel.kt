package com.digioteste.ui.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.digioteste.data.RepositoryService
import com.digioteste.data.ServiceRepository
import com.digioteste.model.Repository
import com.digioteste.model.RepositoryResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RepositoryViewModel(application: Application) : AndroidViewModel(application) {
    private lateinit var mutableLiveLista: MutableLiveData<RepositoryResponse<Repository>>

    fun getRepositoryObservable(): MutableLiveData<RepositoryResponse<Repository>> {
        if (!::mutableLiveLista.isInitialized) {
            mutableLiveLista = MutableLiveData()
        }
        return mutableLiveLista
    }

    fun getRepository() {
        val agendaService = ServiceRepository.createService(RepositoryService::class.java)
        val call = agendaService.repository()

        call.enqueue(object : Callback<Repository> {
            override fun onResponse(
                call: Call<Repository>,
                response: Response<Repository>
            ) {
                try {
                    if (response.isSuccessful) {
                        val repositoryResponse  = RepositoryResponse<Repository>()
                        repositoryResponse.data = response.body()
                        repositoryResponse.message = response.message()
                        repositoryResponse.resultCode = response.code()

                        mutableLiveLista.value = repositoryResponse
                    } else {
                        val repositoryResponse: RepositoryResponse<Repository> =
                            RepositoryResponse(
                                null,
                                response.code(),
                                ""
                            )

                        mutableLiveLista.value = repositoryResponse
                    }
                } catch (e: Exception) {
                    val repositoryResponse: RepositoryResponse<Repository> =
                        RepositoryResponse(
                            null,
                            0,
                            ""
                        )

                    mutableLiveLista.value = repositoryResponse
                }
            }

            override fun onFailure(call: Call<Repository>, t: Throwable) {
                val repositoryResponse: RepositoryResponse<Repository> =
                    RepositoryResponse(
                        null, 503, ""
                    )

                mutableLiveLista.value = repositoryResponse
            }
        })
    }
}