package com.digioteste.ui.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.digioteste.R
import com.digioteste.databinding.ActivityMainBinding
import com.digioteste.ui.adapter.ProductsAdapter
import com.digioteste.ui.adapter.SpotligtAdapter
import com.digioteste.ui.viewmodel.RepositoryViewModel
import com.digioteste.util.ResultCode
import com.digioteste.util.transformation
import com.squareup.picasso.Picasso

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private val repositoryViewModel: RepositoryViewModel by lazy {
        ViewModelProviders.of(this).get(RepositoryViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        addObservable()
        addObservableWithOnCreate()
    }

    private fun addObservable() {
        repositoryViewModel.getRepository()
    }

    private fun addObservableWithOnCreate(){

        repositoryViewModel.getRepositoryObservable().observe(this, Observer { repositoryResponse ->

            if(repositoryResponse != null) {
                when(repositoryResponse.resultCode) {
                    ResultCode.SUCESS.value -> {
                        binding.vpPage.adapter = SpotligtAdapter(this, repositoryResponse.data!!.spotlight!!)

                        binding!!.viewPagerIndicator.setupWithViewPager(binding!!.vpPage)
                        binding!!.viewPagerIndicator.addOnPageChangeListener(listener)
                        binding.tvCash.text = repositoryResponse.data!!.cash!!.title
                        Picasso.get().load(repositoryResponse.data!!.cash!!.bannerURL)
                            .transform(transformation()).into(binding.ivCash)
                        binding!!.rvProducts.setHasFixedSize(true)
                        binding!!.rvProducts.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
                        binding!!.rvProducts.adapter = ProductsAdapter(this, repositoryResponse.data!!.products!!)
                    }
                    ResultCode.NOTFOUND.value -> {
                    }
                }
            }
        })
    }

    private val listener: ViewPager.OnPageChangeListener = object : ViewPager.OnPageChangeListener {
        override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
        override fun onPageSelected(position: Int) {}
        override fun onPageScrollStateChanged(state: Int) {}
    }
}