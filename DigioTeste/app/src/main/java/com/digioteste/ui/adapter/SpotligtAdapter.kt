package com.digioteste.ui.adapter

import android.content.Intent
import android.view.View
import android.widget.ImageView
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.digioteste.model.Spotlight
import com.digioteste.ui.activity.DescriptionActivity
import com.digioteste.ui.activity.MainActivity


class SpotligtAdapter(
    val ctx: MainActivity,
    private val data: ArrayList<Spotlight>
): PagerAdapter() {

    override fun instantiateItem(collection: View, position: Int): Any {
        val myImageView = ImageView(ctx)
        Glide
            .with(ctx)
            .load(data[position].bannerURL)
            .centerCrop()
            .transform(RoundedCorners(32))
            .into(myImageView)
        (collection as ViewPager).addView(myImageView)

//        collection.setOnClickListener {
//            ctx.startActivity(Intent(ctx, DescriptionActivity::class.java).putExtra("PARAM_DESCRIPTION", data[position].description))
//        }

        return myImageView
    }

    override fun destroyItem(collection: View, position: Int, view: Any) {
        (collection as ViewPager).removeView(view as View)
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun getCount(): Int {
        return data.size
    }

}